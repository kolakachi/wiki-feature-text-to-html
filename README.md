## Project Requirements
PHP 7.3+  
Access to CLI    

## About  
A Script that converts a Featured text from Wikisource into a single HTML file by downloading each chapter and concatenating them together.  

## Installation  

Unzip project in your prefered location.  

## Usage  

Using CLI cd into project directory( where project was unzipped)  

The endpoint used for this script is `https://en.wikisource.org/w/api.php `     

Run `php converter.php` and follow the instruction.    

A new `index.html` file will be generated at root directory of this project
