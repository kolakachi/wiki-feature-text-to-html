<?php
/*
    converter.php

    Take Home Test for Wikisource Technical Fellow
    A script that converts a Featured text into a single HTML file by downloading each chapter and 
    concatenating them together.

*/

$title = readline('Enter a page title in Wikisource: ');

if (!$title) {
    die("Missing page title\n");
}

convertFeaturedTextToHTML($title);

function convertFeaturedTextToHTML($title){
    $title = str_replace("_", " ", $title);
    $response = fetchFeaturedText($title);
    $result = validateResponse($response);
    if($result['error']){
        die($result['message']);
    }
    
    storeContents($response, $title);
}

function fetchFeaturedText($title){
    $endPoint = "https://en.wikisource.org/w/api.php";
    $params = [
        "action" => "parse",
        "format" => "json",
        "page" => $title,
    ];
    
    $url = $endPoint . "?" . http_build_query( $params );
    print "fetching $title\n";
    
    $ch = curl_init( $url );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    $output = curl_exec( $ch );
    curl_close( $ch );

    $response = json_decode( $output, true );
    print "fetched $title\n";

    return $response;
}

function validateResponse($response){
    $error = false;
    $message = "";
    if(isset($response['error'])){
        $message = $response['error']['info']."\n";
        $error = true;
        return [
            'error' => $error,
            'message' => $message
        ];
    }
    if(!isset($response['parse'])){
        $message = "Unable to load response\n";
        $error = true;
        return [
            'error' => $error,
            'message' => $message
        ];
    }
    
    if(!isset($response['parse']['text'])){
        $message = "Unable to get page text\n";
        $error = true;
        return [
            'error' => $error,
            'message' => $message
        ];
    }
    
    if(!isset($response['parse']['text']['*'])){
        $message = "Unable to load page text\n";
        $error = true;
        return [
            'error' => $error,
            'message' => $message
        ];
    }

    return [
        'error' => $error,
        'message' => $message
    ];

}

function getChapters($response, $title){
    $content = "";
    if(!isset($response['parse']['links'])){
        return $content;
    }
    $links = $response['parse']['links'];

    foreach($links as $link){
        if(!isset($link['ns']) || !isset($link['*'])){
            continue;
        }
        if($link['ns'] != 0){
            continue;
        }
        $parsedTitle = strtolower($title);
        $parsedLink = strtolower($link['*']);

        if(strpos($parsedLink, $parsedTitle) === false){
            continue;
        }
        $linkresponse = fetchFeaturedText($link['*']);
        $result = validateResponse($linkresponse);
        if($result['error']){
            print $result['message'];
            continue;
        }
        
        $content .= $linkresponse['parse']['text']['*'];
    }
    return $content;
}

function storeContents($response, $title){

    $mainContent = $response['parse']['text']['*'];
    $chapters = getChapters($response, $title);
    $pageContent = $mainContent. $chapters;
    $content = <<<HTMLTEXT
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <title>Document</title>
            </head>
            <body>
                $pageContent
            </body>
        </html>
    HTMLTEXT;
    
    
    
    $HTMLFile = fopen("index.html", "w") or die("Unable to open file!");
    fwrite($HTMLFile, $content);
    fclose($HTMLFile);
    print "Contents stored\n";
}

